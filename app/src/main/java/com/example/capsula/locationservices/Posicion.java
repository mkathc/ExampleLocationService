package com.example.capsula.locationservices;

/**
 * Created by Desarrollo3 on 15/02/2017.
 */

public class Posicion {

    private double latitud, longitud;



    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }


}
